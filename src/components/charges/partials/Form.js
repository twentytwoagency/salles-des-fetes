import React, { Component } from 'react';

export default class ChargeForm extends Component {
  constructor(props) {
    super(props);

    this.state_default = {
      charge_name: '',
      charge_price: 0
    };

    this.state = this.state_default;
  }
  onChange(e) {
    let field = e.target;
    this.setState({
      [field.name]: field.value
    });
  }
  submitForm() {
    let valid = true;
    Object.keys(this.state).map(e => {
      if (this.state[e] === '') valid = false;
    });
    if (this.props.type === 'insert') {
      if (valid && this.props.insertCharge(this.state)) {
        this.setState(this.state_default);
      }
    } else {
      if (valid && this.props.updateCharge(this.state)) {
        
        this.setState(this.state_default, () => {
          this.props.type_insert();
        });
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    if ( (nextProps.type !== this.props.type && nextProps.type === 'update') || 
    (this.state._id && nextProps.selected_charge[0] && nextProps.selected_charge[0]._id !== this.state._id)) {
      console.log('dnajsdknalksjdn');
      this.setState(nextProps.selected_charge[0]);
    }
    if (nextProps.type === 'insert') {
      this.setState(this.state_default);
    }
  }

  render() {
    return (
      <div>
        <span className="card-title black-text">
          {this.props.type === 'insert' ? 'Ajouter' : 'Modifier'} Charge
        </span>
        <div className="row">
          <div className="input-field col s12">
            <input
              id="charge_name"
              name="charge_name"
              type="text"
              className="validate"
              onChange={this.onChange.bind(this)}
              value={this.state.charge_name}
            />
            <label
              htmlFor="charge_name"
              className={
                this.state.charge_name.length > 0 ? 'active' : ''
              }
            >
              Charge
            </label>
          </div>
          <div className="input-field col s12">
            <input
              id="charge_price"
              name="charge_price"
              type="number"
              className="validate"
              onChange={this.onChange.bind(this)}
              value={this.state.charge_price}
            />
            <label
              htmlFor="charge_price"
              className={this.state.charge_price != null ? 'active' : ''}
            >
              Prix Unitaire
            </label>
          </div>
        </div>
        <div className="row">
          <button className="btn" onClick={this.submitForm.bind(this)}>
            {this.props.type === 'insert' ? 'Ajouter' : 'Enregistrer'}
          </button>
          {this.props.type === 'update' ? (
            <button
              className="btn-flat right"
              onClick={e => this.props.type_insert()}
            >
              Annuler
            </button>
          ) : (
            <button
              className="btn-flat right"
              onClick={e => this.setState(this.state_default)}
            >
              Reinitialiser
            </button>
          )}
        </div>
      </div>
    );
  }
}
