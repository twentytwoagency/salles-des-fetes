import React, { Component } from 'react';
import axios from 'axios';
import clients from '../../../data/clients';

class AddClientForm extends Component {
  constructor() {
    super();
    this.state = {
      customer_firstname: '',
      customer_lastname: '',
      customer_phone: ''
    };
  }

  onChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }
  handleSubmit(e) {
    e.preventDefault();
    console.log(this.state);

    let client = {
      customer_firstname: this.state.customer_firstname,
      customer_lastname: this.state.customer_lastname,
      customer_phone: this.state.customer_phone
    };

    axios.post('http://localhost:3000/clients', client).then(response => {
      if (response.status === 200) {
        this.props.selectClient(response.data);
      }
    });
  }
  render() {
    return (
      <div>
        <span className="card-title">Ajouter un client</span>
        <form className="row" onSubmit={this.handleSubmit.bind(this)}>
          <div className="input-field col s6">
            <i className="material-icons prefix">person_add</i>
            <input
              id="customer_firstname"
              name="customer_firstname"
              type="text"
              required="required"
              className="validate"
              onChange={this.onChange.bind(this)}
              value={this.state.customer_firstname}
            />
            <label htmlFor="customer_firstname">Nom du client</label>
          </div>
          <div className="input-field col s6">
            <input
              id="customer_lastname"
              name="customer_lastname"
              required="required"
              type="text"
              className="validate"
              onChange={this.onChange.bind(this)}
              value={this.state.customer_lastname}
            />
            <label htmlFor="customer_lastname">Prénom du client</label>
          </div>
          <div className="input-field col s12">
            <i className="material-icons prefix">phone</i>
            <input
              id="customer_phone"
              name="customer_phone"
              required="required"
              type="text"
              className="validate"
              onChange={this.onChange.bind(this)}
              value={this.state.customer_phone}
            />
            <label htmlFor="customer_phone">Numéro de téléphone</label>
          </div>
          <div className="col s12">
            <button className="btn right">Ajouter</button>
          </div>
        </form>
      </div>
    );
  }
}

class AutocompleteClients extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  setSelectedClient(name) {
    let obj = this.state.clients.filter(c => {
      return c.customer_firstname + ' ' + c.customer_lastname === name;
    });
    let first = _.head(obj);
    if (first._id) {
      this.props.selectClient(first);
    }
  }

  componentDidMount() {
    axios.get('http://localhost:3000/clients').then(response => {
      if (response.status === 200) {
        this.setState({ clients: response.data }, () => {
          let getclientfullname = (() => {
            let data = {};
            this.state.clients.map(e => {
              data[e.customer_firstname + ' ' + e.customer_lastname] = null;
            });
            return data;
          })();
          var elem = document.querySelector('.autocomplete');
          var instance = M.Autocomplete.init(elem, {
            data: getclientfullname,
            onAutocomplete: data => {
              this.setSelectedClient(data);
            }
          });
        });
      }
    });
  }
  onChange() {}

  render() {
    return (
      <div>
        <div className="input-field">
          <i className="material-icons prefix">search</i>
          <input type="text" id="autocomplete-input" className="autocomplete" />
          <label htmlFor="autocomplete-input">Rechercher un client</label>
        </div>
        <h5 className="center-align">OU</h5>
        <AddClientForm selectClient={this.props.selectClient} />
      </div>
    );
  }
}

export default AutocompleteClients;
