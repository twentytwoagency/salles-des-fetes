import React, { Component } from 'react';

export default class ClientForm extends Component {
  constructor(props) {
    super(props);

    this.state_default = {
      customer_firstname: '',
      customer_lastname: '',
      customer_phone: '',
      total_paid: 0
    };

    this.state = this.state_default;
  }
  onChange(e) {
    let field = e.target;

    this.setState({
      [field.name]: field.value
    });
  }
  submitForm() {
    let valid = true;
    Object.keys(this.state).map(e => {
      if (this.state[e] === '') valid = false;
    });
    if (this.props.type === 'insert') {
      if (valid && this.props.insertClient(this.state)) {
        this.setState(this.state_default);
      }
    } else {
      if (valid && this.props.updateClient(this.state)) {
        this.setState(this.state_default, () => {
          this.props.type_insert();
        });
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    
    if ( (nextProps.type !== this.props.type && nextProps.type === 'update') || 
    (this.state._id && nextProps.selected_client[0] && nextProps.selected_client[0]._id !== this.state._id)) {
      this.setState(nextProps.selected_client[0]);
    }
    if (nextProps.type === 'insert') {
      this.setState(this.state_default);
    }
  }

  render() {
    return (
      <div>
        <span className="card-title black-text">
          {this.props.type === 'insert' ? 'Ajouter' : 'Modifier'} client
        </span>
        <div className="row">
          <div className="input-field col s12">
            <input
              id="customer_firstname"
              name="customer_firstname"
              type="text"
              className="validate"
              onChange={this.onChange.bind(this)}
              value={this.state.customer_firstname}
            />
            <label
              htmlFor="customer_firstname"
              className={
                this.state.customer_firstname.length > 0 ? 'active' : ''
              }
            >
              Nom du client
            </label>
          </div>
          <div className="input-field col s12">
            <input
              id="customer_lastname"
              name="customer_lastname"
              type="text"
              className="validate"
              onChange={this.onChange.bind(this)}
              value={this.state.customer_lastname}
            />
            <label
              htmlFor="customer_lastname"
              className={
                this.state.customer_lastname.length > 0 ? 'active' : ''
              }
            >
              Prénom du client
            </label>
          </div>
          <div className="input-field col s12">
            <input
              id="customer_phone"
              name="customer_phone"
              type="text"
              className="validate"
              onChange={this.onChange.bind(this)}
              value={this.state.customer_phone}
            />
            <label
              htmlFor="customer_phone"
              className={this.state.customer_phone.length > 0 ? 'active' : ''}
            >
              Numéro de téléphone
            </label>
          </div>
        </div>
        <div className="row">
          <button className="btn" onClick={this.submitForm.bind(this)}>
            {this.props.type === 'insert' ? 'Ajouter' : 'Enregistrer'}
          </button>
          {this.props.type === 'update' ? (
            <button
              className="btn-flat right"
              onClick={e => this.props.type_insert()}
            >
              Annuler
            </button>
          ) : (
            <button
              className="btn-flat right"
              onClick={e => this.setState(this.state_default)}
            >
              Reinitialiser
            </button>
          )}
        </div>
      </div>
    );
  }
}
