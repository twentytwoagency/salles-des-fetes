import React from 'react';
import ClientHistoryModal from '../partials/Clients_history';
import axios from 'axios';
import ClientForm from './partials/Form';
import Phone from '../../helpers/phone';
import Money from '../../helpers/money';
import clients from '../../data/clients';
import Pagination from 'react-js-pagination';
import Permissions from '../../helpers/permissions';

class Client extends React.Component {
  constructor(props) {
    super(props);
    this.state = props.client;
  }

  insertClient() {
    this.props.insertClient(this.state);
  }
  removeClient() {
    this.props.removeClient(this.state._id);
  }
  updateClient() {
    this.props.type_update(this.state._id);
  }
  openModal() {
    this.props.openModal(this.state);
  }

  render() {
    return (
      <tr key={this.state.customer_firstname + this.state.customer_lastname}>
        <td>{this.state.customer_firstname}</td>
        <td>{this.state.customer_lastname}</td>
        <td>{Phone.format(this.state.customer_phone)}</td>
        <td>{<Money value={this.state.total_paid} />}</td>
        <td>{<Money value={this.state.remaining} />}</td>

        <td className="action-buttons right">
          <button className="btn btn-small" onClick={this.openModal.bind(this)}>
            <i className="material-icons md-18">history</i>
          </button>
          {Permissions.hasPermission(['clients.*', 'clients.edit']) ? (
            <button
              className="btn orange darken-2 btn-small"
              onClick={this.updateClient.bind(this)}
            >
              {/* <i className="material-icons right">C</i> */}
              <i className="material-icons md-18">mode_edit</i>
            </button>
          ) : (
            ''
          )}
          {Permissions.hasPermission(['clients.*', 'clients.edit']) ? (
            <button
              className="btn  red darken-1 btn-small"
              onClick={this.removeClient.bind(this)}
            >
              <i className="material-icons md-18">delete</i>
            </button>
          ) : (
            ''
          )}
        </td>
      </tr>
    );
  }
}

class Clients extends React.Component {
  // get a reference to the element after the component has mounted
  componentDidMount() {
    this.loadData();
    var elem = document.querySelector('.modal');
    var instance = M.Modal.init(elem, {
      onCloseEnd: () => {
        this.onCloseModal();
      }
    });
  }
  loadData(query = '') {
    let search = '';
    if (query) {
      search = '?search=' + query;
    }

    axios.get('http://localhost:3000/clients' + search).then(response => {
      let pagination = this.state.pagination;
      pagination.count = response.data.length;
      pagination.pages = Math.round(pagination.count / pagination.size);

      this.setState({ clients: response.data, pagination: pagination });
    });
  }

  constructor(props) {
    super(props);

    this.state = {
      clients: [],
      form_type: 'insert',
      pagination: {
        page: 1,
        size: 10,
        start: 0,
        end: 0,
        count: 0
      },
      search: ''
    };
    this.insertClient = this.insertClient.bind(this);
    this.removeClient = this.removeClient.bind(this);
    this.updateClient = this.updateClient.bind(this);
    this.type_update = this.type_update.bind(this);
    this.type_insert = this.type_insert.bind(this);
    this.onOpenModal = this.onOpenModal.bind(this);
    this.onCloseModal = this.onCloseModal.bind(this);
    this.loadData = this.loadData.bind(this);
  }

  onSelectClient(client) {}
  /**
   * Open modal to add money
   * @param {Object} payload
   */
  onOpenModal(payload) {
    this.setState({
      isSelected: true,
      payload: payload
    });
    $('.modal').modal('open');
  }

  /**
   * re-init after modal close
   */
  onCloseModal() {
    this.setState({
      isSelected: false,
      payload: {}
    });
  }
  /**
   * Insert client added thru the form
   * @param {Object} client
   */
  insertClient(client) {
    axios.post('http://localhost:3000/clients', client).then(response => {
      if (response.status === 200) {
        let myclients = this.state.clients.slice();
        myclients.push(response.data);

        this.setState({ clients: myclients });
      }
    });
    return true;
  }
  /**
   * Remove the client
   * @param {String | Number} client_id
   */
  removeClient(client_id) {
    axios
      .delete('http://localhost:3000/clients/' + client_id)
      .then(response => {
        if (response.status === 204) {
          let clients = this.state.clients.filter(e => {
            return e._id !== client_id;
          });

          this.setState({ clients: clients });
        }
      });
  }
  /**
   * Update a client using the object returned from the form
   * @param {Object} client
   */
  updateClient(client) {
    axios
      .put('http://localhost:3000/clients/' + client._id, client)
      .then(response => {
        if (response.status === 200) {
          let clients = this.state.clients.map(e => {
            if (e._id === client._id) {
              return response.data;
            } else {
              return e;
            }
          });
          this.setState({
            clients: clients
          });
        }
      });

    return true;
  }
  /**
   * Switch form to update
   */
  type_update(client_id) {
    this.setState({
      form_type: 'update',
      selected_client: client_id
    });
  }
  /**
   * Swith form to insert
   */
  type_insert() {
    this.setState({
      form_type: 'insert',
      selected_client: null
    });
  }

  /**
   * Search query
   */
  search(e) {
    let input = e.target;

    this.loadData(input.value);
  }

  onChange(selectedPage) {
    let old_pagination = this.state.pagination;
    old_pagination.page = selectedPage;
    this.setState({
      pagination: old_pagination
    });
  }

  render() {
    let clients = (
      <tr>
        <td colSpan="5" className="center-align">
          Aucun client ajouté
        </td>
      </tr>
    );

    let list = this.state.clients.slice(
      this.state.pagination.start +
        this.state.pagination.size * (this.state.pagination.page - 1),
      this.state.pagination.start +
        this.state.pagination.size * this.state.pagination.page
    );

    if (list.length) {
      clients = list.map(client => {
        return (
          <Client
            onClick={this.onSelectClient}
            client={client}
            key={client._id}
            removeClient={this.removeClient}
            updateClient={this.updateClient}
            type_update={this.type_update}
            openModal={this.onOpenModal}
          />
        );
      });
    }

    let modal = this.state.isSelected ? (
      <ClientHistoryModal
        payload={this.state.payload}
        update_client={this.updateClient}
        refresh_list={this.refreshList}
        onCloseModal={this.onCloseModal}
      />
    ) : (
      ''
    );
    /**
     * if a client is selected for update, send it
     */

    let selected_client = list.filter(e => {
      return e._id === this.state.selected_client;
    });

    let form = (
      <ClientForm
        insertClient={this.insertClient}
        updateClient={this.updateClient}
        type={this.state.form_type}
        type_insert={this.type_insert}
        selected_client={selected_client}
      />
    );
    let list_class = Permissions.hasPermission(['clients.*', 'clients.edit']) ? 'col s8' : 'col s12'
    return (
      
      <div className="main-container">
        <h3 className="title">Clients</h3>
        <div className="row">
          {Permissions.hasPermission(['clients.*', 'clients.edit']) ? (
            <div className="col s4">
              <div className="card">
                <div className="card-content white-text">{form}</div>
              </div>
            </div>
          ) : (
            
            ''
          )}
          
          <div className={list_class}>
            <div className="row">
              <h4 className="col s8">Liste des clients</h4>
              <div className="col s4">
                <div className="input-field inline right">
                  <input
                    id="search"
                    name="search"
                    type="text"
                    className="validate"
                    onChange={this.search.bind(this)}
                  />
                  <label htmlFor="search">Recherche</label>
                </div>
              </div>
            </div>
            <table className="responsive-table ">
              <thead>
                <tr>
                  <th>Nom</th>
                  <th>Prénom</th>
                  <th>Téléphone</th>
                  <th>Total achat</th>
                  <th>Reste a payer</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>{clients}</tbody>
            </table>
            <Pagination
              activePage={this.state.pagination.page}
              itemsCountPerPage={this.state.pagination.size}
              totalItemsCount={this.state.pagination.count}
              pageRangeDisplayed={11}
              onChange={::this.onChange}
            />
          </div>
        </div>
        <div id="modal1" className="modal">
          {modal}
        </div>
      </div>
    );
  }
}
export default Clients;
