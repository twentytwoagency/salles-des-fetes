import React from 'react';
import Modal from '../partials/Services_modal';
import axios from 'axios';
import ServiceForm from './partials/Form';
import Phone from '../../helpers/phone';
import Money from '../../helpers/money';
import service from '../../data/services';
import Pagination from 'react-js-pagination';
import Permissions from '../../helpers/permissions';

class Service extends React.Component {
  constructor(props) {
    super(props);
    this.state = props.service;
  }

  insertService() {
    this.props.insertService(this.state);
  }
  removeService() {
    this.props.removeService(this.state._id);
  }
  updateService() {
    this.props.type_update(this.state._id);
  }
  componentWillReceiveProps(new_props) {
    if (this.state.service !== new_props) {
      this.setState(new_props.service);
    }
  }

  render() {
    return (
      <tr key={this.state.service_name}>
        <td>{this.state.service_name}</td>
        <td>
          <Money value={this.state.service_price} />
        </td>
        {Permissions.hasPermission(['services.*', 'services.edit']) ? (
          <td className="action-buttons right">
            <button
              className="btn orange darken-2 btn-small"
              onClick={this.updateService.bind(this)}
            >
              {/* <i className="material-icons right">C</i> */}
              <i className="material-icons md-18">mode_edit</i>
            </button>
            <button
              className="btn  red darken-1 btn-small"
              onClick={this.removeService.bind(this)}
            >
              <i className="material-icons md-18">delete</i>
            </button>
          </td>
        ) : (
          <td></td>
        )}
      </tr>
    );
  }
}

class Services extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      services: [],
      form_type: 'insert',
      pagination: {
        page: 1,
        size: 10,
        start: 0,
        end: 0,
        count: 0
      }
    };

    this.onSelectService = this.onSelectService.bind(this);
    this.insertService = this.insertService.bind(this);
    this.removeService = this.removeService.bind(this);
    this.updateService = this.updateService.bind(this);
    this.type_update = this.type_update.bind(this);
    this.type_insert = this.type_insert.bind(this);
    this.loadData = this.loadData.bind(this);
  }
  // get a reference to the element after the component has mounted
  componentDidMount() {
    this.loadData();
    var elem = document.querySelector('.modal');
    var instance = M.Modal.init(elem, {
      onCloseEnd: () => {
        // re-init
        // this.setState({
        // });
      }
    });
  }

  loadData(query = '') {
    let search = '';
    if (query) {
      search = '?search=' + query;
    }
    axios.get('http://localhost:3000/services' + search).then(response => {
      let pagination = this.state.pagination;
      pagination.count = response.data.length;
      pagination.pages = Math.round(pagination.count / pagination.size);

      this.setState({ services: response.data, pagination: pagination });
    });
  }

  onSelectService(service) {
    this.setState({
      isSelected: true,
      payload: service
    });

    $('.modal').modal('open');
  }
  /**
   * Insert service added thru the form
   * @param {Object} service
   */
  insertService(service) {
    axios.post('http://localhost:3000/services', service).then(response => {
      if (response.status === 200) {
        let myServices = this.state.services.slice();
        myServices.push(response.data);
        this.setState({ services: myServices });
      }
    });

    return true;
  }
  /**
   * Remove the service
   * @param {String | Number} service_îd
   */
  removeService(service_id) {
    axios
      .delete('http://localhost:3000/services/' + service_id)
      .then(response => {
        if (response.status === 204) {
          let services = this.state.services.filter(e => {
            return e._id !== service_id;
          });

          this.setState({ services: services });
        }
      });
  }
  /**
   * Update a service using the object returned from the form
   * @param {Object} service
   */
  updateService(service) {
    axios
      .put('http://localhost:3000/services/' + service._id, service)
      .then(response => {
        if (response.status === 200) {
          let services = this.state.services.map(e => {
            if (e._id === service._id) {
              return service;
            } else {
              return e;
            }
          });

          this.setState({
            services: services
          });
        }
      });

    return true;
  }
  /**
   * Switch form to update
   */
  type_update(service_id) {
    this.setState({
      form_type: 'update',
      selected_service: service_id
    });
  }
  /**
   * Swith form to insert
   */
  type_insert() {
    this.setState({
      form_type: 'insert',
      selected_service: null
    });
  }
  search(e) {
    let input = e.target;

    this.loadData(input.value);
  }

  onChange(selectedPage) {
    let old_pagination = this.state.pagination;
    old_pagination.page = selectedPage;
    this.setState({
      pagination: old_pagination
    });
  }

  render() {
    let Services = (
      <tr>
        <td colSpan="5" className="center-align">
          Aucun Services ajouté
        </td>
      </tr>
    );

    let list = this.state.services.slice(
      this.state.pagination.start +
        this.state.pagination.size * (this.state.pagination.page - 1),
      this.state.pagination.start +
        this.state.pagination.size * this.state.pagination.page
    );

    if (list.length) {
      Services = list.map(service => {
        return (
          <Service
            onClick={this.onSelectService}
            service={service}
            key={service._id}
            removeService={this.removeService}
            updateService={this.updateService}
            type_update={this.type_update}
          />
        );
      });
    }

    let modal = this.state.isSelected ? (
      <ServicesModal
        payload={this.state.payload}
        refresh_list={this.refreshList}
        onCloseModal={this.onCloseModal}
      />
    ) : (
      ''
    );
    /**
     * if a service is selected for update, send it
     */

    let selected_service = list.filter(e => {
      return e._id === this.state.selected_service;
    });

    let form = (
      <ServiceForm
        insertService={this.insertService}
        updateService={this.updateService}
        type={this.state.form_type}
        type_insert={this.type_insert}
        selected_service={selected_service}
      />
    );
    let list_class = Permissions.hasPermission(['services.*', 'services.edit'])
      ? 'col s8'
      : 'col s12';
    return (
      <div className="main-container">
        <h3 className="title">Services</h3>
        <div className="row">
          {Permissions.hasPermission(['services.*', 'services.edit']) ? (
            <div className="col s4">
              <div className="card">
                <div className="card-content white-text">{form}</div>
              </div>
            </div>
          ) : (
            ''
          )}

          <div className={list_class}>
            <div className="row">
              <div className="col s8">
                <h4>Liste des Services</h4>
              </div>
              <div className="col s4">
                <div className="input-field inline right">
                  <input
                    id="search"
                    name="search"
                    type="text"
                    className="validate"
                    onChange={this.search.bind(this)}
                  />
                  <label htmlFor="search">Recherche</label>
                </div>
              </div>
            </div>

            <table className="responsive-table ">
              <thead>
                <tr>
                  <th>Services</th>
                  <th>Prix</th>
                  {Permissions.hasPermission(['services.*', 'services.edit']) ? (
                    <th>Actions</th>
                  ) : (
                    <th></th>
                  )}
                </tr>
              </thead>
              <tbody>{Services}</tbody>
            </table>
            <Pagination
              activePage={this.state.pagination.page}
              itemsCountPerPage={this.state.pagination.size}
              totalItemsCount={this.state.pagination.count}
              pageRangeDisplayed={11}
              onChange={::this.onChange}
              nextPageText={<i className="material-icons">chevron_right</i>}
              prevPageText={<i className="material-icons">chevron_left</i>}
            />
          </div>
        </div>
        <div id="modal1" className="modal">
          {modal}
        </div>
      </div>
    );
  }
}
export default Services;
