import React, { Component } from 'react';
import Money from '../../../helpers/money';
import axios from 'axios';
// TODO Important set service price static, in case it changes in the futur
export default class ServicesChips extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selected_services: [],
      services: [],
      reduction: 0
    };
    this.instance;
    this.addChip = this.addChip.bind(this);
    this.removeChip = this.removeChip.bind(this);
  }

  componentDidMount() {
    axios.get('http://localhost:3000/services').then(response => {
      if (response.status === 200) {
        let data = {};

        response.data.map(s => {
          data[s.service_name + ` (${s.service_price} DA)`] = null;
        });
        this.setState({
          services: response.data
        });
        var elem = document.querySelector('.chips');
        this.instance = M.Chips.init(elem, {
          placeholder: 'Ajouter un service',
          secondaryPlaceholder: '+Service',
          autocompleteOptions: {
            data: data,
            limit: Infinity,
            minLength: 1
          },
          onChipAdd: this.addChip,
          onChipDelete: this.removeChip
        });
      } else {
        console.log(response);
      }
    });
  }
  getObjectService(name) {
    let obj = this.state.services.find(service => {
      let formatted_name =
        service.service_name + ` (${service.service_price} DA)`;

      return name === formatted_name;
    });
    if (obj._id && !obj.service_quantity) {
      obj.service_quantity = 1;
    }
    return obj;
  }

  addChip(e, data) {
    let old_state = this.state.selected_services;

    this.instance.chipsData.map(chip => {
      let chip_service = this.getObjectService(chip.tag);
      // if not in state, add it
      if (old_state.indexOf(chip_service) === -1) {
        old_state.push(chip_service);
        this.setState(
          {
            selected_services: old_state
          },
          () => {
            this.props.setServices(
              this.state.selected_services,
              this.state.reduction
            );
          }
        );
      }
    });
  }
  incrementQuantity(obj, e) {
    let input = e.target;
    let old_state = this.state.selected_services;

    let new_state = old_state.map(s => {
      if (obj._id === s._id) {
        s.service_quantity = +input.value;
      }
      return s;
    });

    this.setState(
      {
        selected_services: new_state
      },
      () => {
        this.props.setServices(this.state.selected_services);
      }
    );
  }
  removeChip(e, data) {
    let old_state = this.state.selected_services;
    let removed_chip = {};
    /**
     * for each chip, populate it with its object
     * then verify if it exists in the state
     * if a chip doesn't exists in the chipsdata anymore but still is in the state, remove it
     */

    old_state.map(s => {
      let find = this.instance.chipsData.find(chip => {
        let chip_service = this.getObjectService(chip.tag);
        return chip_service === s;
      });
      if (!find) removed_chip = s;
    });

    let new_state = old_state.filter(s => {
      return s._id !== removed_chip._id;
    });
    this.setState(
      {
        selected_services: new_state
      },
      () => {
        this.props.setServices(this.state.selected_services);
      }
    );
  }
  setReduction(e) {
    let input = e.target,
      value = +input.value;

    this.setState(
      {
        reduction: value
      },
      () => {
        this.props.setServices(
          this.state.selected_services,
          this.state.reduction
        );
      }
    );
  }
  render() {
    let selected_services;
    let selected_services_container;
    let total_selected_services = 0;
    if (this.state.selected_services.length > 0) {
      this.state.selected_services.map(e => {
        total_selected_services += e.service_quantity * e.service_price;
      });
      total_selected_services =
        total_selected_services -
        total_selected_services * this.state.reduction / 100;
      selected_services = this.state.selected_services.map(service => {
        return (
          <tr key={service._id}>
            <td>{service.service_name}</td>
            <td className="center-align">
              <input
                value={service.service_quantity}
                className="center-align"
                type="number"
                data-id={service._id}
                min="1"
                onChange={this.incrementQuantity.bind(this, service)}
              />
            </td>
            <td className="right-align">
              <Money value={service.service_quantity * service.service_price} />
            </td>
          </tr>
        );
      });
      selected_services_container = (
        <div>
          <h5>Liste des services séléctionnés</h5>
          <table className="striped">
            <thead>
              <tr>
                <th width="60%">Nom du service</th>
                <th width="10%" className="center-align">
                  Qte
                </th>
                <th width="30%" className="right-align">
                  Prix
                </th>
              </tr>
            </thead>
            <tbody>{selected_services}</tbody>
            <tfoot>
              <tr>
                <td />
                <td />
                <td className="right-align">
                  Totale:{' '}
                  <Money
                    value={total_selected_services}
                    style={{ fontWeight: 'bold' }}
                  />
                </td>
              </tr>
              <tr>
                <td />
                <td />
                <td className="right-align">
                  <div className="input-field row">
                    Réduction:
                    <input
                      name="reduction"
                      placeholder="Réduction (%)"
                      type="number"
                      className="col s9"
                      value={this.state.reduction}
                      onChange={this.setReduction.bind(this)}
                    />
                    <span className="col s3" style={{ fontSize: '35px' }}>
                      %
                    </span>
                  </div>
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
      );
    }
    return (
      <div>
        <div className="chips chips-autocomplete" />
        {selected_services_container}
      </div>
    );
  }
}
