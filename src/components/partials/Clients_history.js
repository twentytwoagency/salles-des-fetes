import React, { Component } from 'react';
import axios from 'axios';
import moment from 'moment';
import Money from '../../helpers/money';
import accounting from 'accounting';

class Single extends Component {
  constructor(props) {
    super(props);

    this.state = {
      status: {}
    };
    this.getStatus = this.getStatus.bind(this);
  }

  componentDidMount() {
    this.getStatus(this.props.event._id);
  }
  getStatus(_id) {
    return axios
      .get(`http://localhost:3000/events/status/${_id}`)
      .then(response => {
        if (response.status === 200) {
          this.setState({
            status: response.data
          });
        }
      });
  }
  render() {
    let remaining = this.props.total - this.state.status.paid;
    return (
      <td>
        {remaining == 0 ? (
          <span className="new badge" data-badge-caption="">
            Réglé
          </span>
        ) : (
          <span
            className="new badge red"
            data-badge-caption=""
            title={
              'Rest: ' +
              accounting.formatMoney(remaining, {
                symbol: 'DA',
                format: '%v %s'
              })
            }
          >
            Non réglé
          </span>
        )}
      </td>
    );
  }
}

export default class ClientHistoryModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      not_paid: []
    };
    this.getData = this.getData.bind(this);
  }
  componentDidMount() {
    this.getData();
  }
  getData() {
    axios
      .get(`http://localhost:3000/events?customer=${this.props.payload._id}`)
      .then(response => {
        if (response.status === 200) {
          this.setState({
            data: response.data
          });
        }
      });
  }

  render() {
    let history;
    if (this.state.data.length > 0) {
      history = this.state.data.map(e => {
        let total = 0;
        e.services.map(s => {
          total += s.service_price * s.service_quantity;
        });

        return (
          <tr key={e._id}>
            <td>{e.event_type}</td>
            <td>{moment(e.start).format('DD/MM/YYYY à hh[h]mm')}</td>
            <td>
              <Money value={total} />
            </td>
            <Single event={e} total={total} />
          </tr>
        );
      });
    }
    return (
      <div>
        <div className="modal-content">
          <div className="row">
            <h4>
              Historique:{' '}
              <small>
                {this.props.payload.customer_firstname +
                  ' ' +
                  this.props.payload.customer_lastname}
              </small>
            </h4>
            <table>
              <thead>
                <tr>
                  <th>Type d'événement</th>
                  <th>Date</th>
                  <th>Total</th>
                  <th>Statut</th>
                </tr>
              </thead>
              <tbody>{history}</tbody>
            </table>
          </div>
        </div>
        <div className="modal-footer">
          <a
            className="modal-action modal-close waves-effect waves-green btn-flat"
            onClick={e => this.props.onCloseModal()}
          >
            Fermer
          </a>
        </div>
      </div>
    );
  }
}
