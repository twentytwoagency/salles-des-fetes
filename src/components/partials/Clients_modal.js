import React, { Component } from 'react';

export default class ClientsModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0
    }
  }
  onChange(e){
    let input = e.target;
    this.setState({
      [input.name]: input.value
    });
    
  }
  onSubmit(e){
    let client = this.props.payload;
    client.total_paid += +this.state.value;
    client.remaining -= +this.state.value;
    
    if(this.state.value !== 0 && this.props.update_client(client)){
      this.setState({
        value: 0
      })
    }
  }

  render() {
    return (
      <div>
        <div className="modal-content">
          <div className="row">
            <div className="input-field col s12">
              <input
                id="value"
                name="value"
                type="number"
                className="validate"
                onChange={this.onChange.bind(this)}
                value={this.state.value}
              />
              <label
                htmlFor="value"
                className="active"
                >
                Montant vérsé
              </label>
            </div>
          </div>
        </div>
        <div className="modal-footer">
          <a className="modal-action modal-close waves-effect waves-green btn-flat">
            Annuler
          </a>
          <a className="modal-action modal-close waves-effect waves-green btn-flat" onClick={this.onSubmit.bind(this)}>
            Valider
          </a>
        </div>
      </div>
    );
  }
}
