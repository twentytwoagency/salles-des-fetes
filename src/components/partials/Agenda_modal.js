import React from 'react';
import moment from 'moment';

import AutocompleteClients from '../clients/partials/Autocomplete';
import ServicesChip from '../services/partials/Chips';
import axios from 'axios';
moment.locale('fr');
export default class AgendaModal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      start: props.payload.start,
      end:
        props.payload.slots.length > 1
          ? props.payload.end.setHours(23, 59, 59, 999)
          : props.payload.end,
      slots: props.payload.slots,
      selectedTime: props.payload.slots.length > 1 ? 'day' : 'morning',
      customer: {},
      event_type: '',
      services: [],
      payments: [],
      reduction: 0,
      settings: props.settings
    };
    this.setServices = this.setServices.bind(this);
  }

  selectTime(e) {
    if (this.state.slots.length > 1) return;

    let start = this.state.start;

    let end = this.state.end;
    let settings = this.state.settings.times.reservations;
    let time = {};
    switch (e) {
      case 'morning':
        time = {
          start: settings.morning.start.split(':'),
          end: settings.morning.end.split(':')
        };
        start = moment(start)
          .set({ hours: time.start[0], minutes: time.start[1] })
          .toDate();
        end = moment(end)
          .set({ hours: time.end[0], minutes: time.end[1] })
          .toDate();
        break;

      case 'night':
        time = {
          start: settings.night.start.split(':'),
          end: settings.night.end.split(':')
        };
        start = moment(start)
          .set({ hours: time.start[0], minutes: time.start[1] })
          .toDate();
        end = moment(end)
          .set({ hours: time.end[0], minutes: time.end[1] })
          .toDate();
        break;

      case 'day':
        time = {
          start: settings.day.start.split(':'),
          end: settings.day.end.split(':')
        };
        start = moment(start)
          .set({ hours: time.start[0], minutes: time.start[1] })
          .toDate();
        end = moment(end)
          .set({ hours: time.end[0], minutes: time.end[1] })
          .toDate();
        break;
    }

    this.setState(
      {
        start: start,
        end: end,
        selectedTime: e
      },
      () => {}
    );
  }
  selectClient(obj) {
    this.setState({
      customer: obj
    });
  }
  onChange(e) {
    let field = e.target;

    this.setState({
      [field.name]: field.value
    });
  }
  setServices(services, reduction) {
    this.setState({
      services: services,
      reduction: reduction
    });
  }

  submitForm(e) {
    let selected_time = '';
    switch (this.state.selectedTime) {
      case 'morning':
        selected_time = 'Matin';
        break;
      case 'night':
        selected_time = 'Soir';
        break;
      case 'day':
        selected_time = 'Journée compléte';
        break;
    }

    let obj = {
      title:
        this.state.event_type +
        ' ' +
        this.state.customer.customer_firstname +
        ' ' +
        this.state.customer.customer_lastname,
      start: this.state.start,
      end: this.state.end,
      allDay: false
    };

    if (
      this.state.customer._id &&
      this.state.event_type &&
      this.state.services
    ) {
      axios.post('http://localhost:3000/events', this.state).then(response => {
        if (response.status === 200) {
          this.props.refresh_list(response.data);

          $('.modal').modal('close');
          M.toast({
            html: `<div>Résérvation Ajoutée.</div>`,
            classes: 'green'
          });
          
        } else {
          M.toast({ html: `<div>${response.data.err}</div>`, classes: 'red' });
        }
      });
    } else {
      M.toast({
        html: `<div>Veuillez remplire tous le formulaire puis cliquer sur valider.</div>`,
        classes: 'red'
      });
    }
  }

  componentDidMount() {
    var elem = document.querySelector('.collapsible');
    var instance = M.Collapsible.init(elem, {});
    this.selectTime('morning');
  }
  render() {
    let radio_buttons = time => {
      let classes = 'btn ';
      if (this.state.selectedTime === time) {
        classes += 'disabled';
      }
      return this.state.selectedTime === time ? 'btn disabled' : 'btn';
    };

    let reservation_dates = () => {
      let the = this.state.slots.length > 1 ? 'les jours' : 'le';

      let dates = moment(this.state.start).format('dddd Do MMMM YYYY');
      if (this.state.slots.length > 1) {
        dates = (
          <ul>
            {this.state.slots.map(e => {
              return <li key={e}>{moment(e).format('dddd Do MMMM YYYY')}</li>;
            })}
          </ul>
        );
      }

      return (
        <blockquote style={{ borderColor: '#26a69a' }}>
          Résérvation pour {the}: <strong>{dates}</strong>
        </blockquote>
      );
    };
    let customer_card;

    if (this.state.customer && this.state.customer._id) {
      customer_card = (
        <div className="row">
          <div className="card grey lighten-4">
            <div className="card-content">
              <h4>
                <i className="material-icons">person</i>
                {this.state.customer.customer_firstname +
                  ' ' +
                  this.state.customer.customer_lastname}
              </h4>
              <h5>
                <i className="material-icons">phone</i>
                {this.state.customer.customer_phone}
              </h5>
            </div>
          </div>
        </div>
      );
    }

    /**
     * Progression
     */
    let event_progress = 0;
    if (this.state.event_type.length >= 1) {
      event_progress = 1;
    }

    let client_progress = 0;
    if (this.state.customer._id) {
      client_progress = 1;
    }
    let services_progress = 0;

    if (this.state.services.length) {
      services_progress = 1;
    }

    return (
      <div>
        <div className="modal-content">
          <h4>Ajouter une résérvation</h4>
          <div className="row">
            <div className="col s12">
              <div className="row">{reservation_dates()}</div>
              <div className="row">
                <div className="col s12 radio-buttons">
                  <p className="left">Temps de la résérvation: </p>
                  <p className="right">
                    <button
                      className={radio_buttons('morning')}
                      onClick={this.selectTime.bind(this, 'morning')}
                    >
                      Matinée
                    </button>
                    <button
                      className={radio_buttons('night')}
                      onClick={this.selectTime.bind(this, 'night')}
                    >
                      Soirée
                    </button>
                    <button
                      className={radio_buttons('day')}
                      onClick={this.selectTime.bind(this, 'day')}
                    >
                      Journée compléte
                    </button>
                  </p>
                </div>
              </div>
              <div className="row">
                <div className="col s12">
                  <ul className="collapsible">
                    <li>
                      <div className="collapsible-header">
                        <i className="material-icons">event</i>
                        Evenement{' '}
                        <strong>
                          ({event_progress}
                          /1)
                        </strong>
                        {event_progress === 1 ? (
                          <div className="right green-text">
                            <i className="material-icons">check</i>
                          </div>
                        ) : (
                          ''
                        )}
                      </div>
                      <div className="collapsible-body">
                        <div className="input-field">
                          <i className="material-icons prefix">event</i>
                          <input
                            id="event_type"
                            name="event_type"
                            type="text"
                            className="validate"
                            onChange={this.onChange.bind(this)}
                            value={this.state.event_type}
                          />
                          <label htmlFor="event_type">Type d'événement</label>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div className="collapsible-header">
                        <i className="material-icons">person</i>
                        Client{' '}
                        <strong>
                          {' '}
                          ({client_progress}
                          /1)
                        </strong>
                        {client_progress === 1 ? (
                          <div className="right green-text">
                            <i className="material-icons">check</i>
                          </div>
                        ) : (
                          ''
                        )}
                      </div>
                      <div className="collapsible-body">
                        {customer_card}
                        <AutocompleteClients
                          selectClient={this.selectClient.bind(this)}
                        />
                      </div>
                    </li>
                    <li>
                      <div className="collapsible-header">
                        <i className="material-icons">whatshot</i>
                        Services{' '}
                        <strong>
                          {' '}
                          ({services_progress}
                          /1)
                        </strong>
                        {services_progress === 1 ? (
                          <div className="right green-text">
                            <i className="material-icons">check</i>
                          </div>
                        ) : (
                          ''
                        )}
                      </div>
                      <div className="collapsible-body">
                        <ServicesChip setServices={this.setServices} />
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="modal-footer">
          <a className="modal-action modal-close waves-effect waves-green btn-flat">
            Annuler
          </a>
          <a
            className="modal-action waves-effect waves-green btn-flat"
            onClick={this.submitForm.bind(this)}
          >
            Valider
          </a>
        </div>
      </div>
    );
  }
}
