import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import axios from 'axios';
import Logo from '../../assets/images/logo.jpg';
const JWT = require('jsonwebtoken');

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: localStorage.token ? true : false
    };
    
  }
  
  onSubmit(e) {
    e.preventDefault();

    let obj = {
      username: e.target.username.value,
      password: e.target.password.value
    };
    axios.post('http://localhost:3000/users/login', obj).then(response => {
      if (response.status === 200) {
        if (!response.data.err) {
          if(response.data.loggedIn){
            localStorage.setItem('token', response.data.token);
          this.setState({
              loggedIn: true,
            token: localStorage.token
          });
          }else{
            M.toast({ html: response.data.err, classes: 'red' });
          }
          
        } else {
          M.toast({ html: response.data.err, classes: 'red' });
        }
      }
    });
  }

  render() {
    let login_form = (this.state.token || this.state.loggedIn) ? (
      <Redirect
        to={{
          pathname: '/app'
        }}
      />
    ) : (
      <center
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          height: '100vh'
        }}
      >
        <div className="container" style={{ width: '30vw' }}>
        <img src={Logo} style={{maxWidth: '100%', marginBottom: '2rem'}}/>
          <div
            className="z-depth-1 grey lighten-4 row"
            style={{
              display: 'inline-block',
              padding: '1rem 2rem 0px 1rem',
              border: '1px solid #EEE',
              width: '100%'
            }}
          >
            <form className="col s12" onSubmit={this.onSubmit.bind(this)}>
              <div className="row">
                <div className="col s12" />
              </div>

              <div className="row">
                <div className="input-field col s12">
                  <input
                    className="validate"
                    type="text"
                    name="username"
                    id="username"
                  />
                  <label htmlFor="username">Nom d'utilisateur</label>
                </div>
              </div>

              <div className="row">
                <div className="input-field col s12">
                  <input
                    className="validate"
                    type="password"
                    name="password"
                    id="password"
                  />
                  <label htmlFor="password">Mot de passe</label>
                </div>
              </div>

              <br />
              <center>
                <div className="row">
                  <button
                    type="submit"
                    name="btn_login"
                    className="col s12 btn btn-large waves-effect "
                  >
                    Ouvrir
                  </button>
                </div>
              </center>
            </form>
          </div>
            <p className="white-text">Logiciel développé par Agence Digitale 360°</p>
            <strong className="white-text">0671 08 27 72 - 0698 533 115</strong>
            <p className="white-text">www.digitale360.com</p>
        </div>
      </center>
    );
    return (
      <main style={{ height: '100vh' }} className="indigo darken-4">
        {login_form}
      </main>
    );
  }
}
