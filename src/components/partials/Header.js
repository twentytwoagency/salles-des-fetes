import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import Permissions from '../../helpers/permissions';
var JWT = require('jsonwebtoken');
class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: localStorage.token ? true : false
    };
  }

  loggout(e) {
    e.preventDefault();

    localStorage.removeItem('token');
    this.setState({
      loggedIn: false
    });
  }

  render() {
    let header = this.state.loggedIn ? (
      <div className="nav-wrapper">
        <a href="#" className="left brand-logo">
          Salles des fêtes
        </a>
        <ul id="nav-mobile" className="right">
          {Permissions.hasPermission([
            'agenda.*',
            'agenda.edit',
            'agenda.view'
          ]) ? (
            <li>
              <Link to="/app/">Calendrier</Link>
            </li>
          ) : (
            ''
          )}
          {Permissions.hasPermission([
            'clients.*',
            'clients.edit',
            'clients.view'
          ]) ? (
            <li>
              <Link to="/app/clients">Clients</Link>
          </li>
          ) : (
            ''
          )}
          {Permissions.hasPermission([
            'services.*',
            'services.edit',
            'services.view'
          ]) ? (
            <li>
              <Link to="/app/services">Services</Link>
          </li>
          ) : (
            ''
          )}
          
          {Permissions.hasPermission(['statistiques']) ? (
            <li>
              <Link to="/app/statistique">Statistiques</Link>
          </li>
          ) : (
            ''
          )}

          {Permissions.hasPermission(['settings']) ? (
            <li>
              <Link to="/app/reglages">Réglages</Link>
          </li>
          ) : (
            ''
          )}
          
          <li>
            <a href="/">
              <i className="material-icons">power_settings_new</i>
            </a>
          </li>
        </ul>
      </div>
    ) : (
      <Redirect
        to={{
          pathname: '/'
        }}
      />
    );

    return <nav className="indigo darken-4 main-navigation">{header}</nav>;
  }
}
export default Header;
