import React, { Component } from 'react';
import axios from 'axios';
var JWT = require('jsonwebtoken');

export default class UserForm extends Component {
  constructor(props) {
    super(props);
    this.state_default = {
      username: '',
      password: '',
      password_validation: '',
      password_holder: '',
      password_validation_holder: '',
      permissions: [],
      created_by: '',
      created_at: ''
    };
    this.state = this.state_default;
    
  }
  onChange(e) {
    let field = e.target;

    this.setState({
      [field.name]: field.value
    });
  }
  setPasswordHolder(e) {
    let field = e.target;
    let holder = '';

    if (field.name === 'password') {
      holder = 'password_holder';
    }
    if (field.name === 'password_validation') {
      holder = 'password_validation_holder';
    }
    this.setState({
      [holder]: field.value
    });
  }
  setPermission(permission, e) {
    e.preventDefault();
    let old_state = this.state.permissions;
    let new_state = [];

    if (old_state.indexOf(permission) == -1) {
      /**
       * in the case we selected a wrapper permission
       * add edit and view to it
       */

      if (permission.indexOf('.*') !== -1) {
        old_state.push(permission.replace('*', 'view'));
        old_state.push(permission.replace('*', 'edit'));
      } else {
        let page = permission.split('.')[0];
        let current = permission.split('.')[1];
        let search = current === 'view' ? 'edit' : 'view';

        if (old_state.indexOf(page + '.' + search) !== -1) {
          old_state.push(page + '.*');
        }
      }
      old_state.push(permission);
    } else {
      if (permission.indexOf('.*') !== -1) {
        old_state.splice(old_state.indexOf(permission.replace('*', 'view')), 1);
        old_state.splice(old_state.indexOf(permission.replace('*', 'edit')), 1);
      } else {
        let page = permission.split('.')[0];
        let current = permission.split('.')[1];
        let search = current === 'view' ? 'edit' : 'view';
        
        if (old_state.indexOf(page + '.' + search) === -1) {
          old_state.splice(old_state.indexOf(page + '.*'), 1);
        }
      }
      old_state.splice(old_state.indexOf(permission), 1);
    }

    this.setState({
      permissions: old_state
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      (!prevProps.selected_user && this.props.selected_user) ||
      (prevProps.selected_user &&
        this.props.selected_user &&
        prevProps.selected_user !== this.props.selected_user)
    ) {
      let selected_user = this.props.users_list.find(
        e => e._id === this.props.selected_user
      );
      delete selected_user.password;
      this.setState(selected_user);
    }
    if (prevProps.type !== this.props.type) {
      if (this.props.type === 'insert') {
        this.setState(this.state_default);
      }
    }
  }

  onSubmit() {
    let user = JWT.decode(localStorage.token);
    if (this.props.type === 'insert') {
      let obj = {
        username: this.state.username,
        password: this.state.password,
        permissions: this.state.permissions,
        created_by: user.username,
        created_at: new Date()
      };

      axios.post('http://localhost:3000/users', obj).then(response => {
        if (response.status === 200) {
          this.props.refresh_list();
          this.setState(this.state_default);
        }
      });
    } else if (this.props.type === 'update') {
      let valid = true;
      let obj = {
        _id: this.state._id,
        username: this.state.username,
        permissions: this.state.permissions,
        updated_by: user.username,
        updated_at: new Date()
      };
      if (this.state.password.length > 0) {
        if (this.state.password === this.state.password_validation) {
          obj.password = this.state.password;
        } else {
          M.toast({
            html: 'Vérifiez le mot de passe',
            classes: 'red'
          });
          valid = false;
        }
      }

      if (valid) {        
        axios
          .put('http://localhost:3000/users/' + obj._id, obj)
          .then(response => {
            if (response.status === 200) {
              this.props.refresh_list();
              this.props.type_insert();
              if(response.data._id == JWT.decode(localStorage.token)._id){
                localStorage.setItem('token', response.data.token);
              }
              
              this.setState(this.state_default);
              M.toast({
                html: 'Utilisateur modifié avec succés',
                classes: 'green'
              });
            }
          });
      }
    }
  }

  render() {
    let invalid_password = null;
    if (this.state.password.length && this.state.password_validation.length) {
      invalid_password = this.state.password !== this.state.password_validation;
    }
    let passwords_class = '';
    if (invalid_password !== null) {
      passwords_class = invalid_password ? 'invalid' : 'valid';
    }

    return (
      <div className="row">
        <span className="card-title">{this.props.type === 'insert' ? 'Ajouter' : 'Modifier'} un utilisateur</span>
        <small>{this.props.type === 'insert' ? 'Ajoutez' : 'Modifiez'} un utilisateur et selectionnez ses priviléges</small>
        <div className="row">
          <div className="input-field col s12">
            <input
              id="username"
              name="username"
              type="text"
              className="validate"
              onChange={this.onChange.bind(this)}
              value={this.state.username}
            />
            <label
              htmlFor="username"
              className={this.state.username.length > 0 ? 'active' : ''}
            >
              Nom d'utilisateur
            </label>
          </div>
          <div className="input-field col s6">
            <input
              id="password"
              name="password"
              type="password"
              className="validate"
              value={this.state.password_holder}
              className={passwords_class}
              onChange={this.setPasswordHolder.bind(this)}
              onBlur={this.onChange.bind(this)}
            />
            <label htmlFor="password">Mot de passe</label>
          </div>
          <div className="input-field col s6">
            <input
              id="password_validation"
              name="password_validation"
              type="password"
              value={this.state.password_validation_holder}
              className={passwords_class}
              onChange={this.setPasswordHolder.bind(this)}
              onBlur={this.onChange.bind(this)}
            />
            <label htmlFor="password_validation">
              Vérifiez le Mot de passe
            </label>
          </div>
        </div>
        <div className="row">
          <div className="col s12">
            <span className="card-title">Permissions</span>
            <div className="card grey lighten-2">
              <ul className="collection">
                <li className="collection-item">
                  <div>
                    Agenda
                    <a
                      href=""
                      className="secondary-content"
                      onClick={this.setPermission.bind(this, 'agenda.*')}
                    >
                      <i className="material-icons">
                        {this.state.permissions.indexOf('agenda.*') === -1
                          ? 'radio_button_unchecked'
                          : 'radio_button_checked'}
                      </i>
                    </a>
                  </div>
                </li>
                <li className="collection-item">
                  <div>
                    ― Voir
                    <a
                      href=""
                      className="secondary-content"
                      onClick={this.setPermission.bind(this, 'agenda.view')}
                    >
                      <i className="material-icons">
                        {this.state.permissions.indexOf('agenda.view') === -1
                          ? 'radio_button_unchecked'
                          : 'radio_button_checked'}
                      </i>
                    </a>
                  </div>
                </li>
                <li className="collection-item">
                  <div>
                    ― Modifier
                    <a
                      href=""
                      className="secondary-content"
                      onClick={this.setPermission.bind(this, 'agenda.edit')}
                    >
                      <i className="material-icons">
                        {this.state.permissions.indexOf('agenda.edit') === -1
                          ? 'radio_button_unchecked'
                          : 'radio_button_checked'}
                      </i>
                    </a>
                  </div>
                </li>
                <li className="collection-item">
                  <div>
                    Clients
                    <a
                      href=""
                      className="secondary-content"
                      onClick={this.setPermission.bind(this, 'clients.*')}
                    >
                      <i className="material-icons">
                        {this.state.permissions.indexOf('clients.*') === -1
                          ? 'radio_button_unchecked'
                          : 'radio_button_checked'}
                      </i>
                    </a>
                  </div>
                </li>
                <li className="collection-item">
                  <div>
                    ― Voir
                    <a
                      href=""
                      className="secondary-content"
                      onClick={this.setPermission.bind(this, 'clients.view')}
                    >
                      <i className="material-icons">
                        {this.state.permissions.indexOf('clients.view') === -1
                          ? 'radio_button_unchecked'
                          : 'radio_button_checked'}
                      </i>
                    </a>
                  </div>
                </li>
                <li className="collection-item">
                  <div>
                    ― Modifier
                    <a
                      href=""
                      className="secondary-content"
                      onClick={this.setPermission.bind(this, 'clients.edit')}
                    >
                      <i className="material-icons">
                        {this.state.permissions.indexOf('clients.edit') === -1
                          ? 'radio_button_unchecked'
                          : 'radio_button_checked'}
                      </i>
                    </a>
                  </div>
                </li>
                <li className="collection-item">
                  <div>
                    Services
                    <a
                      href=""
                      className="secondary-content"
                      onClick={this.setPermission.bind(this, 'services.*')}
                    >
                      <i className="material-icons">
                        {this.state.permissions.indexOf('services.*') === -1
                          ? 'radio_button_unchecked'
                          : 'radio_button_checked'}
                      </i>
                    </a>
                  </div>
                </li>
                <li className="collection-item">
                  <div>
                    ― Voir
                    <a
                      href=""
                      className="secondary-content"
                      onClick={this.setPermission.bind(this, 'services.view')}
                    >
                      <i className="material-icons">
                        {this.state.permissions.indexOf('services.view') === -1
                          ? 'radio_button_unchecked'
                          : 'radio_button_checked'}
                      </i>
                    </a>
                  </div>
                </li>
                <li className="collection-item">
                  <div>
                    ― Modifier
                    <a
                      href=""
                      className="secondary-content"
                      onClick={this.setPermission.bind(this, 'services.edit')}
                    >
                      <i className="material-icons">
                        {this.state.permissions.indexOf('services.edit') === -1
                          ? 'radio_button_unchecked'
                          : 'radio_button_checked'}
                      </i>
                    </a>
                  </div>
                </li>
                <li className="collection-item">
                  <div>
                    Statistiques
                    <a
                      href=""
                      className="secondary-content"
                      onClick={this.setPermission.bind(this, 'statistiques')}
                    >
                      <i className="material-icons">
                        {this.state.permissions.indexOf('statistiques') === -1
                          ? 'radio_button_unchecked'
                          : 'radio_button_checked'}
                      </i>
                    </a>
                  </div>
                </li>
                <li className="collection-item">
                  <div>
                    Réglages
                    <a
                      href=""
                      className="secondary-content"
                      onClick={this.setPermission.bind(this, 'settings')}
                    >
                      <i className="material-icons">
                        {this.state.permissions.indexOf('settings') === -1
                          ? 'radio_button_unchecked'
                          : 'radio_button_checked'}
                      </i>
                    </a>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col s12">
            <button className="btn right" onClick={this.onSubmit.bind(this)}>
              Enregistrer
            </button>
            <button
              className="btn-flat right"
              onClick={e => this.props.type_insert()}
            >
              Reinitialiser
            </button>
          </div>
        </div>
      </div>
    );
  }
}
