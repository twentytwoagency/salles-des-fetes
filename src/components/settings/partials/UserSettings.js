import React, { Component } from 'react';
import axios from 'axios';
import UserForm from './UserForm';
export default class UserSettings extends React.Component {
  constructor(props) {
    super();
    this.state = {
      form_type: 'insert',
      users: []
    };
    this.type_insert = this.type_insert.bind(this);
    this.refresh_list = this.getUsers.bind(this);
  }
  componentDidMount() {
    this.getUsers();
  }
  
  getUsers() {
    axios.get('http://localhost:3000/users').then(response => {
      if (response.status === 200) {
        this.setState({
          users: response.data
        });
      }
    });
  }
  set_edit_user(user) {

    delete user.password;

    this.setState({
      form_type: 'update',
      selected_user: user
    });
  }

  type_insert() {
    this.setState({
      form_type: 'insert',
      selected_user: null
    });
  }
  refresh_list(){
      this.getUsers();
  }
  removeUser(user_id) {
      
    axios.delete('http://localhost:3000/users/' + user_id).then(response => {
      if (response.status === 204) {
        let users = this.state.users.filter(e => {
          return e._id !== user_id;
        });
        
        this.setState({ users: users }, ()=>{
            console.log(this.state.users);
        });
      }
    });
  }

  render() {
    let users = (
      <tr>
        <td colSpan="5" className="center-align">
          Aucun client ajouté
        </td>
      </tr>
    );
    let list = this.state.users;
    
    if (list.length) {
      users = list.map(e => {
        
        return (
          <tr key={e._id}>
            <td>
              {e.username}
            </td>
            <td className="action-buttons right">
              <button
                className="btn orange darken-2 btn-small"
                onClick={this.set_edit_user.bind(this, e._id)}
              >
                <i className="material-icons md-18">mode_edit</i>
              </button>
              <button
                className="btn  red darken-1 btn-small"
                onClick={this.removeUser.bind(this, e._id)}
              >
                <i className="material-icons md-18">delete</i>
              </button>
            </td>
          </tr>
        );
      });
    }

   

    return (
      <div className="row">
        <h3 className="col s12">Utilisateurs</h3>
        <div className="col s6">
          <div className="card  grey lighten-3">
            <div className="card-content">
              <span className="card-title">Liste des utilisateurs</span>

              <div className="row">
                <table className="responsive-table ">
                  <thead>
                    <tr>
                      <th>Pseudo</th>

                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>{users}</tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div className="col s6">
          <div className="card  grey lighten-3">
            <div className="card-content">
              <UserForm
                insertUser={this.insertUser}
                updateUser={this.updateUser}
                type={this.state.form_type}
                type_insert={this.type_insert}
                selected_user={this.state.selected_user}
                users_list = {list}
                refresh_list = {this.refresh_list}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
