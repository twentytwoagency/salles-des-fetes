import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import TimePicker from '../../helpers/timepicker';
import Range from '../../helpers/range';
import Check from '../../helpers/check';
import UserSettings from './partials/UserSettings';

import _ from 'lodash';
window.jQuery = window.$ = require('jquery');
require('../../assets/js/vendor/materialize.min');

class setting extends React.Component {
  componentDidMount() {
    this.getData();
    /**
     * Floating buttons to Save or restore
     */
    var elem = document.querySelector('.fixed-action-btn');
    var instance = M.FloatingActionButton.init(elem, {
      direction: 'left'
    });

    /**
     *
     */
    $('.tooltipped').tooltip();
  }
  constructor(props) {
    super(props);
    this.state = {
      salle_name: '',
      salle_phone: '',
      salle_address: '',
      ticket_note: '',
      times: {
        salle: {
          start: '',
          end: ''
        },

        reservations: {
          morning: {
            start: '',
            end: ''
          },
          night: {
            start: '',
            end: ''
          },
          day: {
            start: '',
            end: ''
          }
        }
      }
    };

    this.updateState = this.updateState.bind(this);
  }

  getData() {
    axios.get('http://localhost:3000/settings').then(response => {
      this.setState(response.data,()=>{
        M.updateTextFields();
      });
    });
  }
  updateInfos(e) {
    
    let input = e.target;
    
    this.setState({
      [input.name]: input.value
    });
  }

  updateState(obj) {
    let new_state;

    if (typeof obj.path === 'object') {
      obj.path.map(e => {
        new_state = _.set(this.state, e, obj.value);
      });
    } else {
      new_state = _.set(this.state, obj.path, obj.value);
    }
    this.setState(new_state);
  }

  saveState(e) {
    e.preventDefault();

    axios
      .put('http://localhost:3000/settings/' + this.state._id, this.state)
      .then(response => {
        if (response.status === 200) {
          this.setState(response.data);
        }
      });
  }
  resetState(e) {
    e.preventDefault();
    this.getData();
  }

  render() {
    return (
      <div className="main-container settings">
        <div className="row">
          <h3 className="col s12">Informations</h3>
          <div className="col s6">
            <div className="card grey lighten-3">
              <div className="card-content">
                <span className="card-title">Informations de la salle</span>
                <div className="row">
                  <div className="input-field col s12">
                    <input
                      id="salle_name"
                      name="salle_name"
                      value={this.state.salle_name}
                      type="text"
                      className="validate"
                      onChange={this.updateInfos.bind(this)}
                    />
                    <label htmlFor="salle_name">Nom de la salle</label>
                  </div>
                  <div className="input-field col s12">
                    <input
                      id="salle_phone"
                      name="salle_phone"
                      value={this.state.salle_phone}
                      type="text"
                      className="validate"
                      onChange={this.updateInfos.bind(this)}
                    />
                    <label htmlFor="salle_phone">Noméro de téléphone</label>
                  </div>
                  <div className="input-field col s12">
                    <input
                      id="salle_address"
                      name="salle_address"
                      value={this.state.salle_address}
                      type="text"
                      className="validate"
                      onChange={this.updateInfos.bind(this)}
                    />
                    <label htmlFor="salle_address">Adresse de la salle</label>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col s6">
            <div className="card grey lighten-3">
              <div className="card-content">
                <span className="card-title">Tickets</span>
                <div className="row">
                  <div className="input-field col s12">
                    <textarea
                      id="ticket_note"
                      name="ticket_note"
                      value={this.state.ticket_note}
                      type="text"
                      className="validate materialize-textarea"
                      onChange={this.updateInfos.bind(this)}
                    >
                    </textarea>
                    <label htmlFor="ticket_note">Note de bas de page</label>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <h3 className="col s12">Horaires</h3>
          <div className="col s4">
            <div className="card  grey lighten-3">
              <div className="card-content">
                <span className="card-title">Ouverture / Fermeture</span>
                <small>
                  Selectionnez les heures d'ouverture et de fermeture de la
                  salle.
                </small>
                <div className="row">
                  <TimePicker
                    container_class="input-field col s12"
                    obj={{
                      path: [
                        'times.salle.start',
                        'times.reservations.morning.start',
                        'times.reservations.day.start'
                      ],
                      value: this.state.times.salle.start
                    }}
                    updateState={this.updateState}
                    name="Début"
                  />
                  <TimePicker
                    container_class="input-field col s12"
                    obj={{
                      path: [
                        'times.salle.end',
                        'times.reservations.day.end',
                        'times.reservations.night.end'
                      ],
                      value: this.state.times.salle.end
                    }}
                    updateState={this.updateState}
                    name="Fin"
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="col s8">
            <div className="card  grey lighten-3">
              <div className="card-content">
                <span className="card-title">
                  Répartition des plages de temps
                </span>
                <small>Selectionnez la division de la journée</small>
                <div className="row">
                  <Range
                    container_class="input-field col s12"
                    obj={{
                      path: [
                        'times.reservations.morning.end',
                        'times.reservations.night.start'
                      ],
                      value: this.state.times.reservations.morning.end
                    }}
                    min={this.state.times.salle.start || 8}
                    max={this.state.times.salle.end || 23}
                    updateState={this.updateState}
                    name="Début"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>

        <UserSettings />

        <div className="fixed-action-btn">
          <a className="btn-floating btn-large red">
            <i className="large material-icons">assignment_turned_in</i>
          </a>
          <ul>
            <li>
              <a
                className="btn-floating grey tooltipped"
                data-position="top"
                data-tooltip="Réinitialiser"
                onClick={this.resetState.bind(this)}
              >
                <i className="material-icons">restore</i>
              </a>
            </li>
            <li>
              <a
                className="btn-floating green tooltipped"
                onClick={this.saveState.bind(this)}
                data-position="top"
                data-tooltip="Enregistrer"
              >
                <i className="material-icons">save</i>
              </a>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

export default setting;
