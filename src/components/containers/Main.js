import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import Header from '../partials/Header';
import Login from '../partials/Login';
import Agenda from '../agenda/Agenda';
import Clients from '../clients/Clients';
import Services from '../services/Services';
import Setting from '../settings/setting';
import Statistique from '../statistique/statistique';
import Charges from '../charges/Charges';

const Auth = {
  isAuthenticated: localStorage.getItem('token') ? true : false,
  authenticate(cb) {
    this.isAuthenticated = true;
    setTimeout(cb, 100);
  },
  signout(cb) {
    this.isAuthenticated = false;
    setTimeout(cb, 100);
  }
};

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => <Component {...props} />} />
);

class Main extends React.Component {
  render() {
    return (
      <main>
        <Switch>
          <Route exact path="/" component={Login} />
          <Route path="/app" component={App} />
        </Switch>
      </main>
    );
  }
}
const App = () => (
  <div style={{backgroundColor: '#FFF'}}>
    <Header />
    <PrivateRoute exact path="/app" component={Agenda} />
    <PrivateRoute exact path="/app/clients" component={Clients} />
    <PrivateRoute exact path="/app/services" component={Services} />
    <PrivateRoute exact path="/app/charges" component={Charges} />
    <PrivateRoute exact path="/app/statistique" component={Statistique} />
    <PrivateRoute exact path="/app/reglages" component={Setting} />
  </div>
);

export default Main;
