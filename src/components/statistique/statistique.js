import React from 'react';
import axios from 'axios';
import MoneyStatistique from './partials/Money';
import ClientStatistique from './partials/Client';
import EventsStatistique from './partials/Events';
import EventTypeStatistique from './partials/EventType';
import ServicesStatistique from './partials/Services';
import Datepicker from '../../helpers/datepicker';
import _ from 'lodash';
window.jQuery = window.$ = require('jquery');
require('../../assets/js/vendor/materialize.min');

class Statistique extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      statistique_Type: 'Money',
      times: {
        start: '',
        end: ''
      },
      data: {
        labels: [
          'January',
          'February',
          'March',
          'April',
          'May',
          'June',
          'July'
        ],
        datasets: [
          {
            label: 'Clients',
            backgroundColor: 'rgb(255, 99, 132)',
            borderColor: 'rgb(255, 99, 132)',
            data: [0, 10, 5, 2, 20, 30, 45]
          }
        ]
      }
    };
  }
  selectType(e, selectedtype) {
    console.log(selectedtype);
    selectedtype.preventDefault();
    this.setState({ statistique_Type: e });
  }
  componentDidMount() {
    this.getData();
  }
  getData() {
    axios.get('http://localhost:3000/clients').then(response => {
      this.setState(response.data);
    });
  }
  render() {
    let collection_item = CurrentType => {
      let classes = 'collection-item ';
      return this.state.statistique_Type === CurrentType
        ? 'collection-item active'
        : 'collection-item';
    };
    return (
      <div className="main-container settings">
        <div className="row">
          <div className="col s4">
            <div className="card  grey lighten-3">
              <div className="card-content">
                <span className="card-title">Statistiques</span>
                <small>Selectionnez Le type des Statistiques.</small>
                <div className="row">
                  <div className="collection">
                    <a
                      className={collection_item('Money')}
                      onClick={this.selectType.bind(this, 'Money')}
                    >
                      Paiements
                    </a>
                    <a
                      className={collection_item('Client')}
                      onClick={this.selectType.bind(this, 'Client')}
                    >
                      Clients
                    </a>
                    <a
                      className={collection_item('Event')}
                      onClick={this.selectType.bind(this, 'Event')}
                    >
                      Reservation
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col s8">
            {
              {
                Service: <ServicesStatistique />,
                EventType: <EventTypeStatistique />,
                Event: <EventsStatistique />,
                Client: <ClientStatistique />,
                Money: <MoneyStatistique />,
                default: <ClientStatistique />
              }[this.state.statistique_Type]
            }
          </div>
        </div>
      </div>
    );
  }
}

export default Statistique;
