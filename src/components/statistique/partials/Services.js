import React, { Component } from 'react';
import {Bar} from 'react-chartjs-2';
import DateSelector from "../partials/DateSelector";
import Card from '../../../helpers/card'
export default class ServiceStatistique extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            range: {
              start: '',
              end: ''
            },
            data: {
              labels: [
                'Tables',
                'Serveur',
                'DJ',
                'Dinner',
                'Decoration',
                'Sevices',
                'Cuisine'
              ],
              datasets: [
                {
                  label: 'Nbr Services',
                  //backgroundColor: 'rgb(' + Math.floor(Math.random() * 255) + ',' + Math.floor(Math.random() * 255) + ',' + Math.floor(Math.random() * 255) + ')',
                  borderColor: 'rgb(' + Math.floor(Math.random() * 255) + ',' + Math.floor(Math.random() * 255) + ',' + Math.floor(Math.random() * 255) + ')',
                  data: [90, 50, 7, 89, 40, 33, 12], 
                  backgroundColor: [
                    "#FF6384",
                    "#36A2EB",
                    "#FFCE56",
                    "#FACE36",
                    "#b8436d", "#00d9f9", "#a4c73c", "#a4add3"
                ]
                }
              ]
            }
          };

          this.updateDate = this.updateDate.bind(this);
    }
    
    updateDate(date) {
        this.setState({
          range: date
        });
      }
    render() {
        return (
            <div className="card  grey lighten-3">
            <div className="card-content">
              <span className="card-title">Statistiques Services</span>
              <div className="row">
                
                  <DateSelector updateState={this.updateDate} />
                  <div className="row">
                    {/* <Card
                            container_class="col s4 center-align"
                            icon="view_list"
                            name="Sevices "
                            value='8000'
                    />
                    <Card
                            container_class="col s4 center-align"
                            icon="money_off"
                            name="Reste à payer"
                            value='13000'
                    />
                    <Card
                            container_class="col s4 center-align"
                            icon="monetization_on"
                            name="Reste à payer"
                            value='200000'
                    /> */}
                    </div>
                  <Bar data={this.state.data} />  
              </div>
              
            </div>
          </div>
        );
    }
}