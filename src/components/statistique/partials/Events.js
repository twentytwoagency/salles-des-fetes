import React, { Component } from 'react';
import { Line } from 'react-chartjs-2';
import axios from 'axios'
import DateSelector from '../partials/DateSelector';
export default class EventsStatistique extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      range: {
        start: '',
        end: ''
      },
      data: {
        labels: [
          'janvier',
          'février',
          'mars',
          'avril',
          'mai',
          'juin',
          'juillet',
          'août',
          'septembre',
          'octobre',
          'novembre',
          'décembre'
        ],
        datasets: [
          {
            label: 'Nbr des evenements',
            //backgroundColor: 'rgb(' + Math.floor(Math.random() * 255) + ',' + Math.floor(Math.random() * 255) + ',' + Math.floor(Math.random() * 255) + ')',
            borderColor: 'rgb(50, 99, 132)',
            data: [90, 50, 7, 89, 40, 33, 12, 300, 38, 79, 30, 100]
          }
        ]
      }
    };

    this.updateDate = this.updateDate.bind(this);
  }
  componentDidMount() {
    axios
      .get('http://localhost:3000/statistics/events/year')
      .then(response => {
        if (response.status === 200) {
          this.setData(response.data);
        }
      });
  }
  loadStatistics(...date) {
    let url = 'http://localhost:3000/statistics/events';
    if (date.length == 2) {
      url += `?start=${date[0]}&end=${date[1]}`;
    }

    axios.get(url).then(response => {
      if (response.status === 200) {
        this.setData(response.data);
      }
    });
  }
  setData(data) {
    let old_state = this.state.data;
    old_state = data;
    this.setState({
      data: old_state
    });
  }
  updateDate(date) {
    console.log(date);
    this.setState(
      {
        range: date
      },
      () => {
        this.loadStatistics(date.start, date.end);
      }
    );
  }
  render() {
    return (
      <div className="card  grey lighten-3">
        <div className="card-content">
          <span className="card-title">Statistiques d'evenements</span>
          <div className="row">
              <DateSelector updateState={this.updateDate} />
              <Line data={this.state.data} />  
          </div>
        </div>
      </div>
    );
  }
}
