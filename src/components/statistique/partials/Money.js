import React from 'react';
import { Pie } from 'react-chartjs-2';
import DateSelector from '../partials/DateSelector';
import Card from '../../../helpers/card';
import axios from 'axios';
import accounting from 'accounting';

class MoneyStatistique extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      range: {
        start: '',
        end: ''
      },
      data: {
        labels: ['Paiement effectué', 'Reste à payer'],
        label: 'Total',
        datasets: [
          {
            data: [500000, 500000],
            backgroundColor: ['#FF6384', '#36A2EB', '#FFCE56', '#FACE36']
          } 
        ]
      }
    };

    this.updateDate = this.updateDate.bind(this);
  }
  componentDidMount() {
    axios.get('http://localhost:3000/statistics/payments').then(response => {
      if (response.status === 200) {
        this.setData(response.data);
      }
    });
  }
  loadStatistics(...date) {
    let url = 'http://localhost:3000/statistics/payments';
    if (date.length == 2) {
      url += `?start=${date[0]}&end=${date[1]}`;
    }

    axios.get(url).then(response => {
      if (response.status === 200) {
        this.setData(response.data);
      }
    });
  }
  setData(data) {
    let old_state = this.state.data;
    old_state = data;

    this.setState({
      data: old_state
    });
  }
  updateDate(date) {
    this.setState(
      {
        range: date
      },
      () => {
        this.loadStatistics(date.start, date.end);
      }
    );
  }
  render() {
    return (
      <div className="card  grey lighten-3">
        <div className="card-content">
          <span className="card-title">Statistiques des Paiments</span>
          <div className="row">
            <DateSelector updateState={this.updateDate} />
          </div>
          <div className="row" />
          <div className="row">
            <div className="col s12">
              <Pie
                data={this.state.data}
                options={{
                  tooltips: {
                    
                    callbacks: {
                      label: (tooltipItem, data) => {
                        var label = data.labels[tooltipItem.index] || '';

                        if (label) {
                            label += ': ';
                        }
                        
                        let money = accounting.formatMoney(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index], {
                            symbol: 'DA',
                            format: '%v %s'
                          });
                        
                        label += money;
                        return label;
                      }
                    }
                  }
                }}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default MoneyStatistique;
