import React, { Component } from 'react';
import Datepicker from '../../../helpers/datepicker';
import moment from 'moment';

export default class DateSelector extends Component {
  constructor(props) {
    super(props);

    this.state = {
      start: '',
      end: '',
      selectedperiode: 'Month' ? 'Month' : 'Year'
    };
    this.updateState = this.updateState.bind(this);
  }
  selectPeriode(e, selected) {
    let date = moment();

    selected.preventDefault();
    if (e == 'Month') {
      this.setState({
        start: date.startOf('Month').format('DD/MM/YYYY'),
        end: date.endOf('Month').format('DD/MM/YYYY')
      });
    } else {
      this.setState({
        start: date.startOf('Year').format('DD/MM/YYYY'),
        end: date.endOf('Year').format('DD/MM/YYYY')
      });
    }
    this.setState(
      {
        selectedperiode: e
      },
      () => {
        if (this.state.start !== '' && this.state.end !== '') {
          this.props.updateState(this.state);
        }
      }
    );
  }

  updateState(obj) {
    let new_state;

    new_state = _.set(this.state, obj.path, obj.value);

    this.setState(new_state, () => {
      if (this.state.start !== '' && this.state.end !== '') {
        this.props.updateState(this.state);
      }
    });
  }
  render() {
    let radio_buttons = selectedcurrent => {
      let classes = 'btn ';
      if (this.state.selectedperiode === selectedcurrent) {
        classes += 'disabled';
      }
      return this.state.selectedperiode === selectedcurrent
        ? 'btn disabled'
        : 'btn';
    };
    return (
      <div className="row">
        <Datepicker
          container_class="input-field col s3"
          obj={{
            path: ['start'],
            value: this.state.start
          }}
          updateState={this.updateState}
          name="Debut"
        />
        <Datepicker
          container_class="input-field col s3"
          obj={{
            path: ['end'],
            value: this.state.end
          }}
          updateState={this.updateState}
          name="Fin"
        />
        <div className="col s6 radio-buttons">
          <p>Periode : </p>
          <button
            className={radio_buttons('Month')}
            onClick={this.selectPeriode.bind(this, 'Month')}
          >
            Mois
          </button>
          <button
            className={radio_buttons('Year')}
            onClick={this.selectPeriode.bind(this, 'Year')}
          >
            Année
          </button>
        </div>
      </div>
    );
  }
}
