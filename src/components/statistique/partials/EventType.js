import React, { Component } from 'react';
import {Doughnut} from 'react-chartjs-2';
import DateSelector from "../partials/DateSelector";

export default class EventTypeStatistique extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      range: {
        start: '',
        end: ''
      },
      data: {
        labels: [
          'Dinner',
          'Evenement',
          'Dejeunée',
          'Marriage',
          'Anniversaire'
        ],
        datasets: [
          {
            label: 'Nbr des Evenements',
            //borderColor: 'rgb(255, 99, 132)',
            data: [90, 50, 7, 89, 40],
            backgroundColor: [
              "#FF6384",
              "#36A2EB",
              "#FFCE56",
              "#FACE36"
          ]
          }
        ]
      }
    };

    this.updateDate = this.updateDate.bind(this);
  }
  updateDate(date) {
    this.setState({
      range: date
    });
  }
  render() {
    return (
      <div className="card  grey lighten-3">
            <div className="card-content">
              <span className="card-title">Statistiques Type d'evenements</span>
              <div className="row">
                
                  <DateSelector updateState={this.updateDate} />
                  <Doughnut data={this.state.data} />  
              </div>
              
            </div>
          </div>
    );
  }
}
