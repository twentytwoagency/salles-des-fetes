import React, { Component } from 'react';
import { Line } from 'react-chartjs-2';
import DateSelector from './DateSelector';
import Card from '../../../helpers/card'
import axios from 'axios';
export default class ClientStatistique extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      range: {
        start: '',
        end: ''
      },
      data: {
        labels: [
          'Janvier',
          'Fevrier',
          'Mars',
          'Avril',
          'Mai',
          'Juin',
          'Juillet',
          'Aout',
          'Septembre',
          'Novembre',
          'Décembre'
        ],
        datasets: [
          {
            label: 'Clients',
            
            borderColor: 'rgb(255, 99, 132)',
            data: [0, 10, 5, 2, 20, 30, 45]
          }
        ]
      }
    };
    this.updateDate = this.updateDate.bind(this);
  }
  componentDidMount() {
    axios
      .get('http://localhost:3000/statistics/clients/year')
      .then(response => {
        if (response.status === 200) {
          this.setData(response.data);
        }
      });
  }
  loadStatistics(...date) {
    let url = 'http://localhost:3000/statistics/clients';
    if (date.length == 2) {
      url += `?start=${date[0]}&end=${date[1]}`;
    }
    
    axios.get(url).then(response => {
        
      if (response.status === 200) {
        console.log(response.data);
        this.setData(response.data);
      }
    });
  }
  setData(data) {
    let old_state = this.state.data;
    old_state = data;
    this.setState({
      data: old_state
    });
  }

  updateDate(date) {
    this.setState(
      {
        range: date
      },
      () => {
          
        this.loadStatistics(date.start, date.end);
      }
    );
  }
  render() {
    return (
      <div className="card  grey lighten-3">
        <div className="card-content">
          <span className="card-title">Statistiques Clients</span>
          <div className="row">
          <DateSelector updateState={this.updateDate} />
          <div className="row">
          {/* <Card 
          container_class="col s4 center-align"
          icon="person"
          name="Clients retourné"
          value='3000'
          />
          <Card 
          container_class="col s4 center-align"
          icon="person_add"
          name="Nouveau Clients"
          value='5000'
          />
          <Card 
          container_class="col s4 center-align"
          icon="group"
          name="Total Clients"
          value='8000'
          /> */}
          </div>
          <Line data={this.state.data} />
          </div>
        </div>
      </div>
    );
  }
}
