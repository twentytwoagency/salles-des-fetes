import React from 'react';
import noUiSlider from 'noUiSlider';
import wNumb from 'wNumb';

export default class Range extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.props;
  }

  onChange(e) {
    let range = e.target;

    this.setState({
      value: range.value
    });
    this.props.updateState(this.state.obj);
  }

  componentWillReceiveProps(new_props) {
    if (new_props.max !== this.props.max || new_props.min !== this.props.min) {
      let css_class = this.state.obj.path[0].replace(/[.]/g, '_');
      let elem = document.querySelector('.' + css_class);
      let unstring = function(str) {
        if (typeof str === 'number') return str;
        let num = 0;
        let splitted = str.split(':');
        let hours = +splitted[0];
        let minutes = +splitted[1] / 60;

        return Math.round(hours + minutes);
      };
      let populateRange = () => {
        let min = new_props.min ? unstring(new_props.min) : 8;
        let max = new_props.max ? unstring(new_props.max) : 23;
        let size = Math.round(max - min);
        let range = {};

        for (let index = min; index <= max; index++) {
          switch (index) {
            case min:
              range['min'] = min;
              break;
            case max:
              range['max'] = max;
              break;

            default:
              let percent = (index - min) * 100 / size + '%';
              range[percent] = index;
              break;
          }
        }
        return range;
      };
      let range = populateRange();
      
      elem.noUiSlider.updateOptions({
        start: unstring(new_props.obj.value),
        range: range
      });
    }
  }

  componentDidMount() {
    let css_class = this.props.obj.path[0].replace(/[.]/g, '_');
    var elem = document.querySelector('.' + css_class);
    
    noUiSlider.create(elem, {
      start: this.state.obj.value,
      connect: [true, false],
      step: 5,
      range: {
        min: this.state.min,
        '10%': 9,
        '20%': 10,
        '30%': 11,
        '40%': 12,
        '50%': 13,
        '60%': 14,
        '70%': 15,
        '80%': 16,
        '90%': 17,
        max: this.state.max
      },
      tooltips: [
        {
          to: value => {
            let minutes_decimal = value % 1;
            let minutes = minutes_decimal * 60;
            let hours = value - minutes_decimal;

            minutes = '00';
            let result = hours + ':' + minutes;
            if (this.state.value !== result) {
              let obj = _.set(this.state, 'obj.value', result);

              this.setState(obj);

              this.props.updateState(this.state.obj);
            }

            return result;
          },
          from: function(value) {
            return value.replace(':', '');
          }
        }
      ],
      pips: {
        mode: 'steps',
        stepped: true,
        density: 5
      }
    });
  }

  render() {
    let _className =
      this.props.obj.path[0].replace(/[.]/g, '_') + ' noUiSlider time-range';
    return (
      <div className={this.props.container_class}>
        {/* <div id="test-slider"></div> */}

        <div className="range-field">
          <label>{this.props.name}</label>

          <div
            type="range"
            className={_className}
            onChange={this.onChange.bind(this)}
          />
        </div>
      </div>
    );
  }
}
