var JWT = require('jsonwebtoken');


export default class Permissions {
    static getUser(){
      let user = JWT.decode(localStorage.token);
      

      return user;
    }
    
    static hasPermission(permissions) {
      
     
  
       
        
        let granted = false;
        _.forEach(permissions, (permission)=>{
          
          if(Permissions.getUser().permissions.indexOf(permission) !== -1){
            
            granted = true;
          }
        });
    
        return granted;
      }


}