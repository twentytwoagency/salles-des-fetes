import React from 'react';


export default class Check extends React.Component {
    constructor(props) {
        super(props);
        this.state = this.props.obj;
    }

    handleChange({target}){
        console.log("target",target);
        console.log("state",this.state);
        
        
        if (target.checked){
           target.removeAttribute('checked');
           target.parentNode.style.textDecoration = "";
           this.setState({
            value: 0
        }) 
        this.props.updateState(this.state);   
        } else {
           target.setAttribute('checked', true);
           target.parentNode.style.textDecoration = "line-through";
           this.setState({
            value: 50
        }) 
        }
    }
    render() {
        return (
            <div>
                <div className={this.props.container_class}>
                <p>
                    <label>
                        <p>{this.props.name}</p>
                        <input type="checkbox" className="filled-in" onClick={this.handleChange.bind(this)}
                        defaultChecked={this.state.Checked}  />
                        <span>{this.props.text}</span>
                    </label>
                </p>
                </div>
            </div>
        );
    }
}