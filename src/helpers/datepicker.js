import React, { Component } from 'react';
import moment from 'moment';
export default class datepicker extends Component {
  constructor(props) {
    super(props);

    this.state = this.props.obj;
  }

  componentDidMount() {
    let css_class = this.props.obj.path[0].replace(/[.]/g, '_');
    var elem = document.querySelector('.' + css_class);
    var instance = M.Datepicker.init(elem, {
      weekdaysAbbrev: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
      today: "Aujourd'hui",
      clear: 'Vider',
      close: 'Ok',
      format: 'dd/mm/yyyy',
      closeOnSelect: true,
      onSelect: datepicked => {
        this.setState({
          value: moment(datepicked).format('DD-MM-YYYY')
        });
      },
      onClose: () => {
        console.log(this.state);
        this.props.updateState(this.state);
      }
    });
  }


  render() {
    return (
      <div className={this.props.container_class}>
        <input
          type="text"
          name={this.props.name}
          className={this.props.obj.path[0].replace(/[.]/g, '_')}
          value={this.props.obj.value}
        />
        <label
          htmlFor={this.props.name}
          className={this.props.obj.value.length > 0 ? 'active' : ''}
        >
          {this.props.name}
        </label>
      </div>
    );
  }
}
