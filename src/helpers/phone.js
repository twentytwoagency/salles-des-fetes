export default class Phone {
    
    static format(number){
        // 0557 073 651
        return number.replace(/(\d{4})(\d{3})(\d{3})/, "$1 $2 $3");
    }



}