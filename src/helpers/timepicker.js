import React, { Component } from 'react';
import moment from 'moment';
export default class TimePicker extends Component {
  constructor(props) {
    super(props);

    this.state = this.props.obj;
  }

  componentDidMount() {
    let css_class = this.props.obj.path[0].replace(/[.]/g, '_');
    var elem = document.querySelector('.' + css_class);
    var instance = M.Timepicker.init(elem, {
      twelveHour: false,
      onCloseEnd: el => {
        if (this.state.value) {
          this.props.updateState(this.state);
        }
      },
      onSelect: (hours, minutes) => {
        let date = moment();
        date.hours(hours);
        date.minutes(minutes);

        this.setState({
          value: date.format('HH:mm')
        });
      }
    });
  }
  render() {
    return (
      <div className={this.props.container_class}>
        <input
          type="text"
          name={this.props.name}
          className={this.props.obj.path[0].replace(/[.]/g, '_')}
          value={this.props.obj.value}
        />
        <label
          htmlFor={this.props.name}
          className={this.props.obj.value.length > 0 ? 'active' : ''}
        >
          {this.props.name}
        </label>
      </div>
    );
  }
}
