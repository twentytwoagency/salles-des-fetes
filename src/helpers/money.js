import React from 'react';

import accounting from 'accounting';

class Money extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: props.value,
      show_currency: +props.show_currency || true,
      style: props.style || {}
    };
  }
  componentWillReceiveProps(new_props) {
    if (new_props.value != this.state.value) {
      this.setState({
        value: +new_props.value
      });
    }
  }

  render() {
    let symbol = this.state.show_currency ? 'DA' : '';
    let money = accounting.formatMoney(this.state.value, {
      symbol: symbol,
      format: '%v %s'
    });

    return <span style={this.state.style}>{money}</span>;
  }
}

export default Money;
