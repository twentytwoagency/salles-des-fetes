import React from 'react';
import PropTypes from 'prop-types';

class Card extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.props.obj;
  }

  render() {
    return (
      <div className={this.props.container_class}>
        <div className="card-panel teal card-statistics">
          <i className="medium material-icons white-text left" style={{lineHeight: "50px"}}>
            {this.props.icon}
          </i>
          <div className="">
            <p>
              <span className="white-text right-align valign">
                {this.props.name}
              </span>
            </p>
            <p>
              <span className="center-align card-value">
                {this.props.value}
              </span>
            </p>
          </div>
        </div>
      </div>
    );
  }
}
export default Card;
